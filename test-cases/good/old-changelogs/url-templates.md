---
significance: significant
synopsis: Add `issue-url-template` and `pr-url-template` config fields
issues: 8
prs: 10
---
changelog-d can now be used on forges other than GitHub by specifying
issue/PR URL templates in the config file like this:

    issue-url-template: https://codeberg.org/{organization}/{repository}/issues/{index}
    pr-url-template: https://codeberg.org/{organization}/{repository}/pulls/{index}
