---
synopsis: Entries can now be specified as Markdown frontmatter.
issues: 3
prs: 5
significance: significant
---

Make sure to start the file with `---`.
Subsequent lines will be parsed as YAML. Do not use `#`, but plain numbers for issues, space separated or as a list.
After another `---`, you may provide the description.
