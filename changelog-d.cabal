cabal-version: 2.2
name:          changelog-d
version:       1.0.2
synopsis:      Concatenate changelog entries into a single one.
category:      Development
description:
  Concate changelog entries into a single file.
  .
  See the README file for more information.
  .
extra-doc-files: ChangeLog.md README.md
license:       GPL-3.0-or-later
license-file:  LICENSE
author:        Oleg Grenrus <oleg.grenrus@iki.fi>
maintainer:    Francesco Gazzetta <fgaz@fgaz.me>
-- To keep CI resource usage reasonable, we only test: oldest supported ghc,
-- ghcup recommended ghc, and newest supported ghc.
tested-with:   GHC ==8.10.7 || ==9.4.8 || ==9.8.2

flag frontmatter
  description: Enable markdown frontmatter support
  manual: True
  default: True

source-repository head
  type:     git
  location: https://codeberg.org/fgaz/changelog-d.git

library changelog-d-internal
  default-language: Haskell2010
  hs-source-dirs:   src

  -- GHC boot libraries
  build-depends:
    , base        >=4.14.0.0 && <4.21
    , bytestring  ^>=0.10.8.2 || ^>=0.11.3.0 || ^>=0.12.0.2
    , Cabal-syntax^>=3.8.1.0 || ^>=3.10.1.0 || ^>=3.12
    , containers  ^>=0.6.5.1 || ^>=0.6.0.1 || ^>=0.7
    , directory   ^>=1.3.6.0
    , filepath    ^>=1.4.2.1 || ^>=1.5
    , pretty      ^>=1.1.3.6

  -- extra dependencies
  build-depends:
    , aeson                  ^>= 2.0 || ^>= 2.1 || ^>= 2.2
    , cabal-install-parsers  ^>=0.3 || ^>=0.4.5 || ^>=0.5 || ^>=0.6
    , generic-lens-lite      ^>=0.1
    , regex-applicative      ^>=0.3.3.1
    , text                   ^>=1.2.4.1 || ^>=2.0 || ^>=2.1
  if flag(frontmatter)
    build-depends: frontmatter ^>=0.1.0

  -- changelog-d: expand src
  exposed-modules:  ChangelogD
  other-extensions:
    DataKinds
    DeriveAnyClass
    DeriveFunctor
    DerivingStrategies
    ExistentialQuantification
    FlexibleContexts
    GeneralizedNewtypeDeriving
    OverloadedStrings
    RankNTypes
    ScopedTypeVariables
    TypeApplications

executable changelog-d
  default-language: Haskell2010
  hs-source-dirs:   cli
  main-is:          Main.hs
  other-modules:    Paths_changelog_d
  autogen-modules:  Paths_changelog_d

  -- dependencies in library
  build-depends:
    , base
    , containers
    , Cabal-syntax
    , changelog-d-internal

  -- extra dependencies
  build-depends:
    optparse-applicative >=0.14.3.0 && <0.16 || >=0.17.0.0 && <0.19
