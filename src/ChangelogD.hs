{-# LANGUAGE CPP                 #-}
{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE DerivingStrategies  #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications    #-}
{-# OPTIONS_GHC -Wall #-}
-- |
-- License: GPL-3.0-or-later
-- Copyright: Oleg Grenrus
--
-- This is a demo application of how you can make Cabal-like
-- file formatter.
--
module ChangelogD (makeChangelog, Opts (..)) where

import Control.Applicative     (some, (<|>))
import Control.Exception       (Exception (..))
import Control.Monad           (unless, void, when)
import Data.Aeson              (FromJSON (parseJSON), withObject, (.:), (.:?), (.!=), withText)
import Data.Char               (isDigit, isSpace)
import Data.Foldable           (for_, traverse_)
#if defined(MIN_VERSION_frontmatter)
import Data.Frontmatter        (parseYamlFrontmatter, IResult (Done, Fail))
#endif
import Data.Function           (on)
import Data.Generics.Lens.Lite (field)
import Data.List               (sort, sortBy)
import Data.Maybe              (isJust, mapMaybe)
import Data.Set                (Set)
import Data.Text               (Text)
import Data.Traversable        (for)
import GHC.Generics            (Generic)
import System.Directory        (listDirectory)
import System.Exit             (exitFailure)
import System.FilePath         ((</>))
import System.IO               (hPutStrLn, stderr)

import qualified Cabal.Parse                     as Parse
import qualified Data.Aeson.Types as A
import qualified Data.ByteString                 as BS
import qualified Data.Map.Strict                 as Map
import qualified Data.Set                        as Set
import qualified Data.Text as T
import qualified Distribution.CabalSpecVersion   as C
import qualified Distribution.Compat.CharParsing as P
import qualified Distribution.FieldGrammar       as C
import qualified Distribution.Fields             as C
import qualified Distribution.Parsec             as C
import qualified Distribution.Pretty             as C
import qualified Distribution.Utils.Generic      as C
import qualified Distribution.Types.PackageName  as C
import qualified Text.PrettyPrint                as PP
import qualified Text.Regex.Applicative          as RE
import qualified Data.Aeson.KeyMap as AKM
import qualified Data.List as L


exitWithExc :: Exception e => e -> IO a
exitWithExc e = do
    hPutStrLn stderr $ displayException e
    exitFailure

makeChangelog :: Opts -> IO ()
makeChangelog opts@Opts {..} = do
    cfg@Cfg{..} <- do
        let filename = optDirectory </> "config"
        contents <- BS.readFile filename
        either exitWithExc return $ Parse.parseWith parseConfig filename contents

    unless (null cfgPackages) $
      for_ optPackage $ \pkg ->
        unless (pkg `elem` cfgPackages) $
          exitWithExc $ PlainError $ "on command line: unknown package " ++ C.prettyShow pkg

    dirContents <- filter (not . isTmpFile) <$> listDirectory optDirectory
    allEntries <- fmap Map.fromList $
      for (filter (/= "config") $ sort dirContents) $ \name -> do
        let fp = optDirectory </> name
        contents <- BS.readFile fp
        entry <- parseEntryFile fp contents
        pure (fp, entry)

    let validationErrors = Map.filter (not . null) $ validateEntry cfg <$> allEntries
    unless (null validationErrors) $ do
      void $ flip Map.traverseWithKey validationErrors $ \fp errors -> do
        hPutStrLn stderr $ "Validation error(s) in entry `" ++ fp ++ "`:"
        for_ errors $ hPutStrLn stderr . ("  "++) . displayException
      exitWithExc $ PlainError "Validation failed."

    let entries = if Set.null optPackage
            then Map.elems allEntries
            else filter (predicate optPackage) $ Map.elems allEntries
          where
            predicate pns e = Set.null (entryPackages e) ||
                              not (Set.disjoint pns $ entryPackages e)

    for_ optPrList $ \prListFP -> do
        ls <- lines <$> readFile prListFP

        let re, reMergeGitHub, reMergeForgejo, reSquash :: RE.RE Char Int
            -- The commit subject can be prefixed
            -- * by four spaces (default)
            -- * by the hash (--pretty=oneline)
            -- * by nothing (--pretty=format:%s)
            -- So we just use reAny
            re = reAny *> (reMergeGitHub <|> reMergeForgejo <|> reSquash)
            reMergeGitHub = "Merge pull request #" *> reIntegral <* " " <* reAny
            -- Also applies to Gitea and Gogs
            reMergeForgejo = "Merge pull request '" *> reAny *> "' (#" *>
                reIntegral <* ") from " <* reAny <* " into " <* reAny
            reSquash = reAny *> "(#" *> reIntegral <* ")"

        let expected :: Set IssueNumber
            expected = Set.fromList $ map IssueNumber $ mapMaybe (RE.match re) ls

        let actual :: Set IssueNumber
            actual = foldMap entryPrs (Map.elems allEntries)

        unless (expected == actual) $ do
            let missing = expected `Set.difference` actual
            unless (Set.null missing) $ do
                hPutStrLn stderr "Missing PRs"
                for_ missing $ \n -> hPutStrLn stderr $ "  - " ++ instantiateTemplate cfg n cfgPrUrlTemplate

            let extra = actual `Set.difference` expected
            unless (Set.null extra) $ do
                hPutStrLn stderr "Extra (unmerged) PRs"
                for_ extra $ \n -> hPutStrLn stderr $ "  - " ++ instantiateTemplate cfg n cfgPrUrlTemplate

    unless (null entries) $ do
        let PerSignificance significant other = groupBySignificance entries

        if null significant
        then
            for_ entries $ \entry ->
                putStr $ formatEntry opts cfg entry
        else do
            putStrLn "### Significant changes\n"

            for_ (sortBy ((packagesCmp `on` entryPackages) <> (flip compare `on` hasDescription)) significant) $ \entry ->
                putStr $ formatEntry opts cfg entry

            putStrLn "### Other changes\n"

            for_ (sortBy (flip compare `on` hasDescription) other) $ \entry ->
                putStr $ formatEntry opts cfg entry

isTmpFile :: FilePath -> Bool
isTmpFile ('.' : _) = True
isTmpFile _ = False

packagesCmp :: Set C.PackageName -> Set C.PackageName -> Ordering
packagesCmp xs ys = compare (length xs) (length ys) <> compare xs ys

-------------------------------------------------------------------------------
-- Validation
-------------------------------------------------------------------------------

data ValidationError = RequiredFieldError RequiredFieldError
                     | UnknownPackage C.PackageName
  deriving anyclass (Exception)
instance Show ValidationError where
  show (RequiredFieldError err) = show err
  show (UnknownPackage pkg) = "Unknown package " ++ C.prettyShow pkg

data RequiredFieldError = MissingRequiredField String
                        | UnknownRequiredField String
instance Show RequiredFieldError where
  show (MissingRequiredField f) = "Missing required field `" ++ f ++ "`"
  show (UnknownRequiredField f) = "Unknown required field `" ++ f ++ "`"

type Validator = Cfg -> Entry -> [ValidationError]

validateEntry :: Validator
validateEntry cfg entry = foldMap (\validator -> validator cfg entry)
  [ validateRequiredFields
  , validatePackages
  ]

validateRequiredFields :: Validator
validateRequiredFields Cfg{..} Entry{..} = fmap RequiredFieldError $
  mapMaybe checkField $ Set.toList cfgRequiredFields

  where
    checkField :: String -> Maybe RequiredFieldError
    checkField reqField = case fieldIsEmpty reqField of
      Left err -> Just err
      Right True -> Just $ MissingRequiredField reqField
      Right False -> Nothing

    fieldIsEmpty "synopsis" = pure $ null entrySynopsis
    fieldIsEmpty "description" = pure $ null entryDescription
    fieldIsEmpty "packages" = pure $ null entryPackages
    fieldIsEmpty "prs" = pure $ null entryPrs
    fieldIsEmpty "issues" = pure $ null entryIssues
    fieldIsEmpty "significance" = pure False -- omitting the field defaults to Other
    fieldIsEmpty f = Left $ UnknownRequiredField f

-- If there are configured packages, ensure all the entry's packages
-- are listed. Otherwise accept any package as with previous behavior.
validatePackages :: Validator
validatePackages Cfg{..} Entry{..}
  | null cfgPackages = []
  | otherwise = fmap UnknownPackage $ Set.toList $ entryPackages Set.\\ cfgPackages

-------------------------------------------------------------------------------
-- Formatting
-------------------------------------------------------------------------------

type UrlTemplate = String

githubIssueTemplate, githubPrTemplate :: UrlTemplate
githubIssueTemplate = "https://github.com/{organization}/{repository}/issues/{index}"
githubPrTemplate = "https://github.com/{organization}/{repository}/pull/{index}"

instantiateTemplate :: Cfg -> IssueNumber -> UrlTemplate -> String
instantiateTemplate cfg (IssueNumber n) =
  RE.replace (fmap getVar $ RE.sym '{' *> some (RE.psym (/='}')) <* RE.sym '}')
  where
    getVar "org"          = cfgOrganization cfg
    getVar "organization" = cfgOrganization cfg
    getVar "repo"         = cfgRepository cfg
    getVar "repository"   = cfgRepository cfg
    getVar "index"        = show n
    getVar var            = "{" ++ var ++ "}"

formatEntry :: Opts -> Cfg -> Entry -> String
formatEntry Opts {..} cfg Entry {..} =
    indent $ header ++ "\n" ++ description
  where
    indent = unlines . indent' . lines
    indent' []     = []
    indent' (x:xs) = ("- " ++ x) : map indentLine xs
    indentLine "" = ""
    indentLine str = "  " ++ str

    header = unwords $
        [ pkgs ++ entrySynopsis
        ] ++
        [ "[" ++ C.prettyShow n ++ "](" ++ instantiateTemplate cfg n (cfgIssueUrlTemplate cfg) ++ ")"
        | n <- Set.toList entryIssues
        ] ++
        [ "[" ++ C.prettyShow n ++ "](" ++ instantiateTemplate cfg n (cfgPrUrlTemplate cfg) ++ ")"
        | n <- Set.toList entryPrs
        ]

    pkgs
        | Set.size optPackage == 1 = ""
        | otherwise
            = concatMap (\pn -> "*" ++ C.unPackageName pn ++ "* ")
            $ Set.toList entryPackages

    description
       | optShort  = ""
       | otherwise = maybe "" (\d -> "\n" ++ trim d ++ "\n\n") entryDescription

trim :: String -> String
trim = tr . tr where tr = dropWhile isSpace . reverse

-------------------------------------------------------------------------------
-- Options
-------------------------------------------------------------------------------

data Opts = Opts
    { optDirectory :: FilePath
    , optShort     :: Bool
    , optPackage   :: Set C.PackageName
    , optPrList    :: Maybe FilePath
    }
  deriving (Show)

-------------------------------------------------------------------------------
-- Auxiliary types
-------------------------------------------------------------------------------

data Significance = Significant | Other
  deriving (Eq, Show)

data PerSignificance a = PerSignificance a a

groupBySignificance :: [Entry] -> PerSignificance [Entry]
groupBySignificance xs = PerSignificance
    (filter ((== Significant) . entrySignificance) xs)
    (filter ((== Other) . entrySignificance) xs)

instance C.Parsec Significance where
    parsec = do
        token <- C.parsecToken
        case token of
            "significant" -> return Significant
            "other"       -> return Other
            _             -> fail $ "Unknown significance: " ++ token

instance C.Pretty Significance where
    pretty Significant = PP.text "significant"
    pretty Other       = PP.text "other"

instance FromJSON Significance where
  parseJSON = withText "Significance" $ \t ->
    case t of
      "significant" -> pure Significant
      "other" -> pure Other
      _ -> fail $ "Could not parse significance: " ++ show t ++ ". Expected one of: significant, other"

newtype IssueNumber = IssueNumber Int
  deriving (Eq, Ord, Show)

instance C.Parsec IssueNumber where
    parsec = do
        _ <- P.char '#'
        IssueNumber <$> P.integral

instance C.Pretty IssueNumber where
    pretty (IssueNumber n) = PP.char '#' PP.<> PP.int n

instance FromJSON IssueNumber where
  parseJSON (A.String s) = parseWithParsec s <|> IssueNumber <$> parseWithParsec' P.integral s
  parseJSON v = IssueNumber <$> parseJSON v

parseWithParsec :: C.Parsec a => Text -> A.Parser a
parseWithParsec = either (fail . show) pure . C.explicitEitherParsec C.parsec . T.unpack

parseWithParsec' :: C.ParsecParser a -> Text -> A.Parser a
parseWithParsec' parser = either (fail . show) pure . C.explicitEitherParsec parser . T.unpack


-------------------------------------------------------------------------------
-- Config
-------------------------------------------------------------------------------

data Cfg = Cfg
    { cfgOrganization :: String
    , cfgRepository   :: String
    , cfgRequiredFields :: Set String
    , cfgIssueUrlTemplate :: UrlTemplate
    , cfgPrUrlTemplate :: UrlTemplate
    , cfgPackages     :: Set C.PackageName
    }
  deriving (Show, Generic)

parseConfig :: [C.Field C.Position] -> C.ParseResult Cfg
parseConfig fields0 = do
    traverse_ parseSection $ concat sections
    C.parseFieldGrammar C.cabalSpecLatest fields cfgGrammar
  where
    (fields, sections) = C.partitionFields fields0

    parseSection :: C.Section C.Position -> C.ParseResult ()
    parseSection (C.MkSection (C.Name pos name) _ _) =
        C.parseWarning pos C.PWTUnknownSection $ "Unknown section " ++ C.fromUTF8BS name

cfgGrammar :: C.ParsecFieldGrammar Cfg Cfg
cfgGrammar = Cfg
    <$> C.uniqueFieldAla   "organization" C.Token (field @"cfgOrganization")
    <*> C.uniqueFieldAla   "repository"   C.Token (field @"cfgRepository")
    <*> C.monoidalFieldAla "required-fields" (C.alaSet' C.FSep C.Token) (field @"cfgRequiredFields")
    <*> C.optionalFieldDefAla "issue-url-template" C.Token (field @"cfgIssueUrlTemplate") githubIssueTemplate
    <*> C.optionalFieldDefAla "pr-url-template"    C.Token (field @"cfgPrUrlTemplate")    githubPrTemplate
    <*> C.monoidalFieldAla "packages"     (C.alaSet C.NoCommaFSep) (field @"cfgPackages")

-------------------------------------------------------------------------------
-- Entry
-------------------------------------------------------------------------------

data Entry = Entry
    { entrySynopsis     :: String
    , entryDescription  :: Maybe String
    , entryPackages     :: Set C.PackageName
    , entryPrs          :: Set IssueNumber
    , entryIssues       :: Set IssueNumber
    , entrySignificance :: Significance
    }
  deriving (Show, Generic)

hasDescription :: Entry -> Bool
hasDescription = isJust . entryDescription

parseEntryFile :: FilePath -> BS.ByteString -> IO Entry
parseEntryFile fp contents = do
  if "---" `BS.isPrefixOf` contents
  then parseEntryFileMD fp contents
  else parseEntryFileCustom fp contents

newtype PlainError = PlainError String
  deriving anyclass (Exception)
instance Show PlainError where
  show (PlainError s) = "error: " ++ s

parseEntryFileMD :: FilePath -> BS.ByteString -> IO Entry
#if defined(MIN_VERSION_frontmatter)
parseEntryFileMD fp contents = do
  case parseYamlFrontmatter contents of
    Done description entry -> do
      return $ entry { entryDescription = Just (C.fromUTF8BS description) }
    Fail _ _ e -> do
      exitWithExc $ PlainError $ "Failed to parse " ++ fp ++ ": " ++ e
    _ -> do
      exitWithExc $ PlainError $ "Failed to parse " ++ fp ++ ": file is incomplete. Make sure to use --- as a separator between frontmatter and description."
#else
parseEntryFileMD _ _ = exitWithExc $ PlainError "File with markdown frontmatter found, but frontmatter support is not available. Please recompile/reinstall changelog-d with the `frontmatter` Cabal flag activated."
#endif

parseEntryFileCustom :: FilePath -> BS.ByteString -> IO Entry
parseEntryFileCustom fp contents = do
    either exitWithExc return $ Parse.parseWith parseEntry fp contents

parseEntry :: [C.Field C.Position] -> C.ParseResult Entry
parseEntry fields0 = do
    traverse_ parseSection $ concat sections
    e <- C.parseFieldGrammar C.cabalSpecLatest fields entryGrammar
    when (null $ entrySynopsis e) $
        C.parseFatalFailure C.zeroPos "Synopsis cannot be empty"
    return e
  where
    (fields, sections) = C.partitionFields fields0

    parseSection :: C.Section C.Position -> C.ParseResult ()
    parseSection (C.MkSection (C.Name pos name) _ _) =
        C.parseWarning pos C.PWTUnknownSection $ "Unknown section " ++ C.fromUTF8BS name

entryGrammar :: C.ParsecFieldGrammar Entry Entry
entryGrammar = Entry
    <$> C.freeTextFieldDef "synopsis"                              (field @"entrySynopsis")
    <*> C.freeTextField    "description"                           (field @"entryDescription")
    <*> C.monoidalFieldAla "packages"     (C.alaSet C.NoCommaFSep) (field @"entryPackages")
    <*> C.monoidalFieldAla "prs"          (C.alaSet C.NoCommaFSep) (field @"entryPrs")
    <*> C.monoidalFieldAla "issues"       (C.alaSet C.NoCommaFSep) (field @"entryIssues")
    <*> C.optionalFieldDef "significance"                          (field @"entrySignificance") Other

instance FromJSON Entry where
  parseJSON = withObject "Entry" $ \v -> do
    case Set.fromList (AKM.keys v) `Set.difference` Set.fromList [
        "synopsis",
        "packages",
        "prs",
        "issues",
        "significance"
      ] of
      unknown | Set.null unknown -> pure ()
              | otherwise -> fail $ "Unknown fields: " ++ L.intercalate ", " [ show key | key <- Set.toList unknown ]
    Entry
      <$> v .: "synopsis"
      <*> pure Nothing -- description must be passed after frontmatter
      <*> (Set.map C.mkPackageName <$> v .:? "packages" .!= mempty)
      <*> (pluralized . splitString <$> v .:? "prs" .!= mempty)
      <*> (pluralized . splitString <$> v .:? "issues" .!= mempty)
      <*> v .:? "significance" .!= Other

newtype SplitString a = SplitString { splitString :: a }
  deriving newtype (Semigroup, Monoid)

instance FromJSON a => FromJSON (SplitString a) where
  parseJSON (A.String s) = SplitString <$> parseJSON (A.toJSONList (A.String <$> T.words s))
  parseJSON v = SplitString <$> parseJSON v

newtype Pluralized a = Pluralized { pluralized :: a }
  deriving newtype (Semigroup, Monoid)

instance FromJSON a => FromJSON (Pluralized a) where
  parseJSON v@(A.Array _) = Pluralized <$> parseJSON v
  parseJSON v = Pluralized <$> parseJSON (A.toJSONList [v])

-------------------------------------------------------------------------------
-- RE extras
-------------------------------------------------------------------------------

reIntegral :: RE.RE Char Int
reIntegral = read <$> some (RE.psym isDigit)

reAny :: RE.RE s [s]
reAny = RE.few RE.anySym
