# changelog-d

**Check and concatenate structured changelog entries into a single one**.

When developing projects with large amounts of in-flight PRs, the changelog file
becomes a primary source of merge conflicts.
Inserting changelog entries in random order and using the union merge strategy
are two solutions to the problem, but the first is not guaranteed to work and
the second is not supported by many forges.
This tool allows you to write changelog entries in separate files and merge
them at release time, avoiding conflicts altogether.

## Usage

### Setup

Create a `changelog.d` directory in your repository, where all changelog entries
will reside.

Create a `config` file inside the `changelog-d` directory with the following contents:

```cabal
organization: your_org_name_here
repository: your_repo_name_here
issue-url-template: https://codeberg.org/{organization}/{repository}/issues/{index}
pr-url-template: https://codeberg.org/{organization}/{repository}/pulls/{index}
required-fields: synopsis description packages prs issues
packages: your_packages_here
```

Only `organization` and `repository` are mandatory.
The url templates will default to GitHub and `required-fields` and `packages` will default to none.
In the templates you can use the placeholders `{organization}`, `{repository}`,
and `{index}` for the forge organization/user, repository, and issue/pr index respectively.

If `packages` is specified, changelog entries and command line parameters will be
validated against the list of packages.

### Adding a changelog entry

Changelog entries are added by creating files in the `changelog.d` directory.

The files follow a simple key-value format similar to the one for `.cabal` files.
Free-form text fields (`synopsis` and `description`) allow Markdown markup.

Here's an example:

```cabal
synopsis: Add feature xyz
packages: somepackage
prs: #102
issues: #100 #101
significance: significant

description: {

- Detail number 1
- Detail number 2

}
```

Only the `synopsis` field is always required, but you should also set the others where applicable.
Some project may require other fields to be present through the `required-fields` config option.

| Field          | Description                                                                                                        |
| -----          | -----------                                                                                                        |
| `synopsis`     | Brief description of the change. Often just the PR title.                                                          |
| `description`  | Longer description, with a list of sub-changes. Not needed for small/atomic changes.                               |
| `packages`     | Packages affected by the change. Omit if it's an overarching or non-package change.  |
| `prs`          | Space-separated hash-prefixed pull request numbers containing the change (usually just one).                       |
| `issues`       | Space-separated hash-prefixed issue numbers that the change fixes/closes/affects.                                  |
| `significance` | Set to `significant` if the change is significant, that is if it warrants being put near the top of the changelog. |

You can find a large number of real-world examples of changelog files
[here](https://github.com/haskell/cabal/tree/bc83de27569fda22dbe1e10be1a921bebf4d3430/changelog.d).

Changelog entries can also be written in markdown-frontmatter format.
This is particularly useful when your entry descriptions conflict with `.cabal` syntax (eg. braces have to be escaped in the `.cabal` format but not in markdown).
To write a markdown-frontmatter entry, start the file with `---`, then write write all the fields except `description` in the yaml frontmatter, and finally write the markdown description after the frontmatter. For example:

```markdown
---
synopsis: Add feature xyz
packages:
  - somepackage
prs: 102
issues: 
  - 100
  - 101
significance: significant
---

- Detail number 1
- Detail number 2

Braces do not cause syntax errors here }}{
```

### Generating a changelog

* Collect a log of merged PRs by running
  `git log --pretty=oneline --no-color $previous_version..HEAD > prlog`.
* Run `changelog-d --prlog prlog`.
  The changelog will be produced on standard output, while PRs with missing
  entries and extra entries will be printed on standard error.

You can generate a changelog for a specific package with the `--package` option (can be used multiple times), and you can change the changelog style with `--long` and `--short`.

See [the workflow used by Cabal](https://github.com/haskell/cabal/wiki/Making-a-release#c2-changelogs) for an example.

## Alternatives and comparison

There are many other tools that perform similar tasks[^1][^2][^3][^4][^5][^6].
`changelog-d` is mainly distinguished by:

* **Opinionated format**.
  `changelog-d` was originally developed for use in [servant](https://www.servant.dev/)
  and later [cabal](https://www.haskell.org/cabal/),
  so it adopted the changelog format used by those projects and supported
  multi-package projects from the start.
* **PR log check**.
  When generating the changelog, `changelog-d` can compare the commit log to
  ensure all PRs have a corresponding entry.

[^1]: https://scriv.readthedocs.io
[^2]: https://changie.dev
[^3]: https://towncrier.readthedocs.io
[^4]: https://github.com/restic/calens
[^5]: https://github.com/hashicorp/go-changelog
[^6]: https://github.com/aklajnert/changelogd
