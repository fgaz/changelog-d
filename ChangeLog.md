## 1.0

- Forked from https://github.com/phadej/changelog-d to https://codeberg.org/fgaz/changelog-d
- Entries can now be specified as Markdown frontmatter. [#3](https://codeberg.org/fgaz/changelog-d/issues/3) [#5](https://codeberg.org/fgaz/changelog-d/pulls/5)

  Make sure to start the file with `---`.
  Subsequent lines will be parsed as YAML. Do not use `#`, but plain numbers for issues, space separated or as a list.
  After another `---`, you may provide the description.

- Add `issue-url-template` and `pr-url-template` config fields [#8](https://codeberg.org/fgaz/changelog-d/issues/8) [#10](https://codeberg.org/fgaz/changelog-d/pulls/10)

  changelog-d can now be used on forges other than GitHub by specifying
  issue/PR URL templates in the config file like this:

      issue-url-template: https://codeberg.org/{organization}/{repository}/issues/{index}
      pr-url-template: https://codeberg.org/{organization}/{repository}/pulls/{index}

- Add `required-fields` config option [#6](https://codeberg.org/fgaz/changelog-d/issues/6) [#9](https://codeberg.org/fgaz/changelog-d/pulls/9)
- Support the Forgejo commit format (commit cfc0c4cd)
- Detect squash-merge commits (commit e798dfcff)

## 0.1

- Initial/rolling release
