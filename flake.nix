{
  description = "Concatenate changelog entries into a single one";

  inputs.get-tested4nix.url = "sourcehut:~fgaz/get-tested4nix";
  inputs.get-tested4nix.inputs.nixpkgs.follows = "nixpkgs";

  outputs = { self, nixpkgs, get-tested4nix }: let
    forAllSystems = nixpkgs.lib.genAttrs nixpkgs.lib.systems.flakeExposed;
    np = system: nixpkgs.legacyPackages."${system}";
  in {
    packages = forAllSystems (system: {
      changelog-d = (np system).haskellPackages.callCabal2nix "changelog-d" ./. {};
      default = self.packages."${system}".changelog-d;
      static = (np system).haskell.lib.justStaticExecutables
        ((np system).pkgsStatic.haskellPackages.callCabal2nix "changelog-d" ./. {});
    });

    checks = forAllSystems (system:
      get-tested4nix.lib.noIFD.callCabal2nixForAllCompilers
        nixpkgs.legacyPackages."${system}"
        ./changelog-d.cabal "changelog-d" ./. {}
    );
  };
}
